/* hash output tool for generating SOE style (broken...) SUID hashes
   along with the more proper way to do so without negatives
   written by Apathy
   improved with output of broken version by Darth 

   License: just don't be a dick
*/

#include <cstdio>
#include <algorithm>
#include <iostream>
#include <string>
#include <utility>
#include <inttypes.h>

int main() {
    std::string name;
    uint32_t uintname = 0;
    int64_t brokenSuidContainer = 0;
    char brokenSuid[50];

    std::cout << "What is your username? ";
    getline(std::cin, name);

    // the default SWG limit is 15 characters long max
    if (name.size() > 15) {
        name.resize(15);
    }

    // make it lower case
    std::transform(name.begin(), name.end(), name.begin(), ::tolower);

    // use std::hash and get the result into a uint32_t, aka uint32 in SWG
    uintname = static_cast<uint32_t>(std::hash < std::string > {}(name.c_str()));

    // the below is a result of sprintf formatting with the hashing above
    sprintf(brokenSuid, "%i", uintname);

    // get the above into an int field, partly to eximplify should we want to add this to the login code
    brokenSuidContainer = std::stoi(brokenSuid);

    std::cout << "Username [" << name << "] StationID [proper: " << uintname << " SOE: " << brokenSuidContainer << "]\n";
}

