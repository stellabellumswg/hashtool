# SUID Hash Tool
## written by Apathy, improved by Darth

The idea is that we needed this to be able to quickly check SUID's from time to time when people can't login, so they can have their account fixed, depending on what SUID generation style is being used.

# Building
    make

# Running
    ./hasher
    What is your username? cekis
    Username [cekis] StationID [proper: 4216143624 SOE: -78823672]

# License
Open source, don't be a dick.
