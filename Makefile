SOURCE=hasher.cpp
BIN=hasher
CC=g++

all: $(BIN)

$(BIN): $(SOURCE)
	$(CC) -m32 $(SOURCE) -o$(BIN)

clean:
	rm -f $(BIN)
